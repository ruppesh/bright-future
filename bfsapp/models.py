from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User



class TimeStamp(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_at = models.DateTimeField(null=True, blank=True)

    class Meta:
        abstract = True

    def delete(self):
        self.deleted_at = timezone.now()
        super().save()





class Blog(TimeStamp):
    title = models.CharField(max_length=500)
    image = models.ImageField(upload_to='blog/')
    content = models.TextField()
    author = models.CharField(max_length=200)

    def __str__(self):
        return self.title



class Video(TimeStamp):
    title = models.CharField(max_length=200)
    url = models.CharField(max_length=255)


    def __str__(self):
        return self.title



class Event(TimeStamp):
    title = models.CharField(max_length=200)
    content = models.TextField()
    image = models.ImageField(upload_to='event/')
    date = models.DateTimeField()
    location = models.CharField(max_length=300)


    def __str__(self):
        return self.title




class Notice(TimeStamp):
    title = models.CharField(max_length=200)
    content = models.TextField()
    date = models.DateTimeField()

    def __str__(self):
        return self.title


class Staff(TimeStamp):
    name = models.CharField(max_length=200)
    image = models.ImageField(upload_to='staff/')
    post = models.CharField(max_length=100)
    phone1 = models.CharField(max_length=20, null=True, blank=True)
    email = models.CharField(max_length=50, null=True, blank=True)
    subject = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return self.name



class Features(TimeStamp):
    title = models.CharField(max_length=200)
    content = models.TextField()
    image = models.ImageField(upload_to='feature/')

    def __str__(self):
        return self.title



class Gallery(TimeStamp):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to='gallery/')

    def __str__(self):
        return self.title


class Slider(TimeStamp):
    image = models.ImageField(upload_to='slider/')
    caption = models.CharField(max_length=255)

    def __str__(self):
        return self.image.url


class Activities(TimeStamp):
    title = models.CharField(max_length=200)
    content = models.TextField()
    image = models.ImageField(upload_to='activities/')
    date = models.DateTimeField()
    location = models.CharField(max_length=300)


    def __str__(self):
        return self.title


class Testimonial(TimeStamp):
    name = models.CharField(max_length=200)
    post = models.CharField(max_length=50)
    says = models.TextField()

    def __str__(self):
        return self.name


class Service(TimeStamp):
    title = models.CharField(max_length=100)
    image = models.ImageField(upload_to='service/', null=True, blank=True)
    content = models.TextField()

    def __str__(self):
        return self.title



class Download(TimeStamp):
    title = models.CharField(max_length=100)
    document = models.FileField(upload_to='documents/')
    content = models.TextField()
    image = models.ImageField(upload_to='service/')
    uploaded_at = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return self.title


class Message(TimeStamp):
    name = models.CharField(max_length=255)
    email = models.EmailField()
    phone = models.CharField(max_length=15, null=True, blank=True)
    subject = models.CharField(max_length=200)
    message = models.TextField()

    def __str__(self):
        return self.name



class Result(TimeStamp):
    grade = models.CharField(max_length=100)
    document = models.FileField(upload_to='documents/')

    def __str__(self):
        return self.grade


class Routine(TimeStamp):
    grade = models.CharField(max_length=100)
    document = models.FileField(upload_to='documents/')

    def __str__(self):
        return self.grade


class Admission(TimeStamp):
    name = models.CharField(max_length=255)
    birthdate =models.CharField(max_length=255, null=True, blank=True)
    email = models.EmailField()
    phone = models.CharField(max_length=15, null=True, blank=True)
    Applicant_PP_size_Photo = models.ImageField(null=True, blank=True, upload_to='admission/')
    Applicant_Marksheet_Photo = models.ImageField(null=True, blank=True, upload_to='admission/')
    grade = models.CharField(max_length=20, null=True, blank=True)
    percentage = models.CharField(max_length=20)
    address = models.CharField(max_length=100)
    muncipilaty = models.CharField(max_length=100)
    district = models.CharField(max_length=100)
    zipcode = models.PositiveIntegerField(null=True, blank=True)

    def __str__(self):
        return self.name


class Vacancy(TimeStamp):
    name = models.CharField(max_length=255)
    birthdate =models.CharField(max_length=255, null=True, blank=True)
    email = models.EmailField()
    phone = models.CharField(max_length=15, null=True, blank=True)
    Applicant_PP_size_Photo = models.ImageField(null=True, blank=True, upload_to='vacancy/')
    Applicant_Marksheet_Photo = models.ImageField(null=True, blank=True, upload_to='vacancy/')
    Qualification = models.CharField(max_length=20, null=True, blank=True)
    percentage = models.CharField(max_length=20)
    subject = models.CharField(max_length=255, null=True, blank=True)
    address = models.CharField(max_length=100)
    muncipilaty = models.CharField(max_length=100)
    district = models.CharField(max_length=100)
    zipcode = models.PositiveIntegerField(null=True, blank=True)

    def __str__(self):
        return self.name




class Vacancii(TimeStamp):
    title = models.TextField()
    content = models.TextField()


    def __str__(self):
        return self.title



class Price(TimeStamp):
    title = models.CharField(max_length=100)
    content = models.TextField()

    def __str__(self):
        return self.title

class A1(TimeStamp):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to='gallery/')

    def __str__(self):
        return self.title

class A2(TimeStamp):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to='gallery/')

    def __str__(self):
        return self.title

class A3(TimeStamp):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to='gallery/')

    def __str__(self):
        return self.title

class A4(TimeStamp):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to='gallery/')

    def __str__(self):
        return self.title

class A5(TimeStamp):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to='gallery/')

    def __str__(self):
        return self.title

class A6(TimeStamp):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to='gallery/')

    def __str__(self):
        return self.title

class A7(TimeStamp):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to='gallery/')

    def __str__(self):
        return self.title

class A8(TimeStamp):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to='gallery/')

    def __str__(self):
        return self.title


    