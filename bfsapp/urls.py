from django.urls import path
from .views import *

app_name='bfsapp'
urlpatterns = [

# admin side
    path('school-admin/logout/', AdminLogout.as_view(),
        name='adminlogout'),
    path('school-admin/login/', AdminLogin.as_view(),
        name='adminlogin'),
    path('school-admin/', AdminHomeView.as_view(), name='adminhome'),



    path('school-admin/videos/',
        AdminVideoListView.as_view(), name='adminvideolist'),
    path('school-admin/videos/create/',
        AdminVideoCreateView.as_view(), name='adminvideocreate'),
    path('school-admin/videos/<int:pk>/update/',
        AdminVideoUpdateView.as_view(), name='adminvideoupdate'),
    path('school-admin/videos/<int:pk>/delete/',
        AdminVideoDeleteView.as_view(), name='adminvideodelete'),



    path('school-admin/blogs/',
        AdminBlogListView.as_view(), name='adminbloglist'),
    path('school-admin/blogs/create/',
        AdminBlogCreateView.as_view(), name='adminblogcreate'),
    path('school-admin/blogs/<int:pk>/update/',
        AdminBlogUpdateView.as_view(), name='adminblogupdate'),
    path('school-admin/blogs/<int:pk>/delete/',
        AdminBlogDeleteView.as_view(), name='adminblogdelete'),

    path('school-admin/events/',
        AdminEventListView.as_view(), name='admineventlist'),
    path('school-admin/events/create/',
        AdminEventCreateView.as_view(), name='admineventcreate'),
    path('school-admin/events/<int:pk>/update/',
        AdminEventUpdateView.as_view(), name='admineventupdate'),
    path('school-admin/events/<int:pk>/delete/',
        AdminEventDeleteView.as_view(), name='admineventdelete'),

    path('school-admin/gallery/',
        AdminGalleryListView.as_view(), name='admingallerylist'),
    path('school-admin/gallery/create/',
        AdminGalleryCreateView.as_view(), name='admingallerycreate'),
    path('school-admin/gallery/<int:pk>/update/',
        AdminGalleryUpdateView.as_view(), name='admingalleryupdate'),
    path('school-admin/gallery/<int:pk>/delete/',
        AdminGalleryDeleteView.as_view(), name='admingallerydelete'),

    path('school-admin/notices/',
        AdminNoticeListView.as_view(), name='adminnoticelist'),
    path('school-admin/notices/create/',
        AdminNoticeCreateView.as_view(), name='adminnoticecreate'),
    path('school-admin/notices/<int:pk>/update/',
        AdminNoticeUpdateView.as_view(), name='adminnoticeupdate'),
    path('school-admin/notices/<int:pk>/delete/',
        AdminNoticeDeleteView.as_view(), name='adminnoticedelete'),

    path('school-admin/staffs/',
        AdminStaffListView.as_view(), name='adminstafflist'),
    path('school-admin/staffs/create/',
        AdminStaffCreateView.as_view(), name='adminstaffcreate'),
    path('school-admin/staffs/<int:pk>/update/',
        AdminStaffUpdateView.as_view(), name='adminstaffupdate'),
    path('school-admin/staffs/<int:pk>/delete/',
        AdminStaffDeleteView.as_view(), name='adminstaffdelete'),

    path('school-admin/testimonials/',
        AdminTestimonialListView.as_view(), name='admintestimoniallist'),
    path('school-admin/testimonials/create/',
        AdminTestimonialCreateView.as_view(), name='admintestimonialcreate'),
    path('school-admin/testimonials/<int:pk>/update/',
        AdminTestimonialUpdateView.as_view(), name='admintestimonialupdate'),
    path('school-admin/testimonials/<int:pk>/delete/',
        AdminTestimonialDeleteView.as_view(), name='admintestimonialdelete'),


    path('school-admin/activities/',
        AdminActivitiesListView.as_view(), name='adminactivitieslist'),
    path('school-admin/activities/create/',
        AdminActivitiesCreateView.as_view(), name='adminactivitiescreate'),
    path('school-admin/activities/<int:pk>/update/',
        AdminActivitiesUpdateView.as_view(), name='adminactivitiesupdate'),
    path('school-admin/activities/<int:pk>/delete/',
        AdminActivitiesDeleteView.as_view(), name='adminactivitiesdelete'),



    path('school-admin/features/',
        AdminFeaturesListView.as_view(), name='adminfeatureslist'),
    path('school-admin/features/create/',
        AdminFeaturesCreateView.as_view(), name='adminfeaturescreate'),
    path('school-admin/features/<int:pk>/update/',
        AdminFeaturesUpdateView.as_view(), name='adminfeaturesupdate'),
    path('school-admin/features/<int:pk>/delete/',
        AdminFeaturesDeleteView.as_view(), name='adminfeaturesdelete'),




    path('school-admin/services/',
        AdminServicesListView.as_view(), name='adminserviceslist'),
    path('school-admin/services/create/',
        AdminServicesCreateView.as_view(), name='adminservicescreate'),
    path('school-admin/services/<int:pk>/update/',
        AdminServicesUpdateView.as_view(), name='adminservicesupdate'),
    path('school-admin/services/<int:pk>/delete/',
        AdminServicesDeleteView.as_view(), name='adminservicesdelete'),



    path('school-admin/download/',
        AdminDownloadListView.as_view(), name='admindownloadlist'),
    path('school-admin/download/create/',
        AdminDownloadCreateView.as_view(), name='admindownloadcreate'),
    path('school-admin/download/<int:pk>/update/',
        AdminDownloadUpdateView.as_view(), name='admindownloadupdate'),
    path('school-admin/download/<int:pk>/delete/',
        AdminDownloadDeleteView.as_view(), name='admindownloaddelete'),



    path('school-admin/routine/',
        AdminRoutineListView.as_view(), name='adminroutinelist'),
    path('school-admin/routine/create/',
        AdminRoutineCreateView.as_view(), name='adminroutinecreate'),
    path('school-admin/routine/<int:pk>/update/',
        AdminRoutineUpdateView.as_view(), name='adminroutineupdate'),
    path('school-admin/routine/<int:pk>/delete/',
        AdminRoutineDeleteView.as_view(), name='adminroutinedelete'),




    path('school-admin/result/',
        AdminResultListView.as_view(), name='adminresultlist'),
    path('school-admin/result/create/',
        AdminResultCreateView.as_view(), name='adminresultcreate'),
    path('school-admin/result/<int:pk>/update/',
        AdminResultUpdateView.as_view(), name='adminresultupdate'),
    path('school-admin/result/<int:pk>/delete/',
        AdminResultDeleteView.as_view(), name='adminresultdelete'),



    path('school-admin/slider/',
        AdminSliderListView.as_view(), name='adminsliderlist'),
    path('school-admin/slider/create/',
        AdminSliderCreateView.as_view(), name='adminslidercreate'),
    path('school-admin/slider/<int:pk>/update/',
        AdminSliderUpdateView.as_view(), name='adminsliderupdate'),
    path('school-admin/slider/<int:pk>/delete/',
        AdminSliderDeleteView.as_view(), name='adminsliderdelete'), 



    path('school-admin/vacancii/',
        AdminVacanciiListView.as_view(), name='adminvacanciilist'),
    path('school-admin/vacancii/create/',
        AdminVacanciiCreateView.as_view(), name='adminvacanciicreate'),
    path('school-admin/vacancii/<int:pk>/update/',
        AdminVacanciiUpdateView.as_view(), name='adminvacanciiupdate'),
    path('school-admin/vacancii/<int:pk>/delete/',
        AdminVacanciiDeleteView.as_view(), name='adminvacanciidelete'),



    path('school-admin/price/',
        AdminPriceListView.as_view(), name='adminpricelist'),
    path('school-admin/price/create/',
        AdminPriceCreateView.as_view(), name='adminpricecreate'),
    path('school-admin/price/<int:pk>/update/',
        AdminPriceUpdateView.as_view(), name='adminpriceupdate'),
    path('school-admin/price/<int:pk>/delete/',
        AdminPriceDeleteView.as_view(), name='adminpricedelete'),
  




    path('school-admin/messages/',
        AdminMessageListView.as_view(), name='adminmessagelist'),
    path('school-admin/messages/<int:pk>/',
        AdminMessageDetailView.as_view(), name='adminmessagedetail'),
    path('school-admin/messages/<int:pk>/delete/',
        AdminMessageDeleteView.as_view(), name='adminmessagedelete'),



    path('school-admin/admission/',
        AdminAdmissionListView.as_view(), name='adminadmissionlist'),
    path('school-admin/admission/<int:pk>/',
        AdminAdmissionDetailView.as_view(), name='adminadmissiondetail'),
    path('school-admin/admission/<int:pk>/delete/',
        AdminAdmissionDeleteView.as_view(), name='adminadmissiondelete'),


    path('school-admin/vacancy/',
        AdminVacancyListView.as_view(), name='adminvacancylist'),
    path('school-admin/vacancy/<int:pk>/',
        AdminVacancyDetailView.as_view(), name='adminvacancydetail'),
    path('school-admin/vacancy/<int:pk>/delete/',
        AdminVacancyDeleteView.as_view(), name='adminvacancydelete'),

     







    # client side


    path('', ClientHomeView.as_view(), name='clienthome'),
	path('home', HomeView.as_view(), name='home'),
	path('about/', ClientAboutView.as_view(), name='clientabout'),
    path('service/', ClientServiceListView.as_view(), name='clientservicelist'),
	path('gallery/', ClientGalleryListView.as_view(), name='clientgallerylist'),
	path('blog/', ClientBlogListView.as_view(), name='clientbloglist'),
    path('blogs/<int:pk>/', ClientBlogDetailView.as_view(), name='clientblogdetail'),
	path('activities/', ClientActivitiesListView.as_view(), name='clientactivitieslist'),
    path('activiti/<int:pk>/', ClientActivitiesDetailView.as_view(), name='clientactivitiesdetail'),
    path('contact/', ClientContactView.as_view(), name='clientcontact'),
    path('events/<int:pk>/', ClientEventDetailView.as_view(), name='clienteventdetail'),
    path('admission/', ClientAdmissionView.as_view(), name='clientadmission'),
    path('admissionn/', ClientAdmissionnView.as_view(), name='clientadmissionn'),
    path('feestructure/', ClientFeestructureView.as_view(), name='clientfeestructure'),
    path('download/', ClientDownloadListView.as_view(), name='ClientdownloadList'),
    path('routine/', ClientRoutineListView.as_view(), name='Clientroutinelist'),
    path('result/', ClientResultListView.as_view(), name='Clientresultlist'),
    path('vacancy/', ClientVacancyListView.as_view(), name='clientvacancylist'),
    path('search/', ClientSearchView.as_view(), name='clientsearch'),
    path('exam/', ClientExamView.as_view(), name='clientexam'),
    path('homework/', ClientHomeworkView.as_view(), name='clienthomework'),
    path('book/', ClientBookView.as_view(), name='clientbook'),
    path('solution/', ClientSolutionView.as_view(), name='clientsolution'),
    path('me/', ClientRuppeshView.as_view(), name='clientruppesh'),
    path('thankum/', ClientThankumView.as_view(), name='clientthankum'),
    path('thankua/', ClientThankuaView.as_view(), name='clientthankua'),
    path('thankuv/', ClientThankuvView.as_view(), name='clientthankuv'),


]