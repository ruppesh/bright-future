from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import *
from .models import *
from .forms import *
from django.db.models import Q
from django.conf import settings
from django.core.mail import send_mail
from urllib.parse import urlparse, parse_qs




# Admin side



class AdminLogout(LoginRequiredMixin, View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect('/')


class AdminLogin(FormView):
    template_name = "bfsapp/admintemplates/adminlogin.html"
    form_class = AdminLoginForm
    success_url = '/school-admin/'

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)
        if user is not None and user.is_superuser:
            login(self.request, user)
        else:
            return render(self.request, self.template_name, {
                'form': form,
                'errors': 'Incorrect username or password'})
        return super().form_valid(form)





class AdminMixin(LoginRequiredMixin, object):
    login_url = '/school-admin/login/'

    def get_context_data(self, *args, **kwargs):
        context = super(AdminMixin, self).get_context_data(*args, **kwargs)
        # context['organization'] = Organization.objects.get(id=1)
        return context


class DeleteMixin(LoginRequiredMixin, UpdateView):
    login_url = '/school-admin/login/'

    def form_valid(self, form):
        form.instance.deleted_at = timezone.now()
        form.save()
        return super().form_valid(form)


class AdminHomeView(AdminMixin, TemplateView):
    template_name = 'bfsapp/admintemplates/adminhome.html'




class AdminVideoListView(AdminMixin, ListView):
    template_name = 'bfsapp/admintemplates/adminvideolist.html'
    queryset = Video.objects.filter(deleted_at__isnull=True).order_by('-id')
    context_object_name = 'allvideos'


class AdminVideoCreateView(AdminMixin, CreateView):
    template_name = 'bfsapp/admintemplates/adminvideocreate.html'
    form_class = AdminVideoCreateForm
    success_url = reverse_lazy('bfsapp:adminvideolist')


    def form_valid(self, form):
        url = form.cleaned_data['url']
        url_data = urlparse(url)
        query = parse_qs(url_data.query)
        video = query['v'][0]
        form.instance.url = video
        form.save()
        return super().form_valid(form)



class AdminVideoUpdateView(AdminMixin, UpdateView):
    template_name = 'bfsapp/admintemplates/adminvideocreate.html'
    model = Video
    form_class = AdminVideoCreateForm
    success_url = reverse_lazy('bfsapp:adminvideolist')


class AdminVideoDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bfsapp/admintemplates/adminvideodelete.html'
    model = Video
    form_class = AdminVideoDeleteForm
    success_url = reverse_lazy('bfsapp:adminvideolist')




class AdminBlogListView(AdminMixin, ListView):
    template_name = 'bfsapp/admintemplates/adminbloglist.html'
    queryset = Blog.objects.filter(deleted_at__isnull=True).order_by('-id')
    context_object_name = 'allblogs'


class AdminBlogCreateView(AdminMixin, CreateView):
    template_name = 'bfsapp/admintemplates/adminblogcreate.html'
    form_class = AdminBlogCreateForm
    success_url = reverse_lazy('bfsapp:adminbloglist')


class AdminBlogUpdateView(AdminMixin, UpdateView):
    template_name = 'bfsapp/admintemplates/adminblogcreate.html'
    model = Blog
    form_class = AdminBlogCreateForm
    success_url = reverse_lazy('bfsapp:adminbloglist')


class AdminBlogDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bfsapp/admintemplates/adminblogdelete.html'
    model = Blog
    form_class = AdminBlogDeleteForm
    success_url = reverse_lazy('bfsapp:adminbloglist')


class AdminEventListView(AdminMixin, ListView):
    template_name = 'bfsapp/admintemplates/admineventlist.html'
    queryset = Event.objects.filter(deleted_at__isnull=True).order_by('-id')
    context_object_name = 'allevents'


class AdminEventCreateView(AdminMixin, CreateView):
    template_name = 'bfsapp/admintemplates/admineventcreate.html'
    form_class = AdminEventCreateForm
    success_url = reverse_lazy('bfsapp:admineventlist')


class AdminEventUpdateView(AdminMixin, UpdateView):
    template_name = 'bfsapp/admintemplates/admineventcreate.html'
    model = Event
    form_class = AdminEventCreateForm
    success_url = reverse_lazy('bfsapp:admineventlist')


class AdminEventDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bfsapp/admintemplates/admineventdelete.html'
    model = Event
    form_class = AdminEventDeleteForm
    success_url = reverse_lazy('bfsapp:admineventlist')


class AdminGalleryListView(AdminMixin, ListView):
    template_name = 'bfsapp/admintemplates/admingallerylist.html'
    queryset = Gallery.objects.filter(deleted_at__isnull=True).order_by('-id')
    context_object_name = 'allgallery'


class AdminGalleryCreateView(AdminMixin, CreateView):
    template_name = 'bfsapp/admintemplates/admingallerycreate.html'
    form_class = AdminGalleryCreateForm
    success_url = reverse_lazy('bfsapp:admingallerylist')


class AdminGalleryUpdateView(AdminMixin, UpdateView):
    template_name = 'bfsapp/admintemplates/admingallerycreate.html'
    model = Gallery
    form_class = AdminGalleryCreateForm
    success_url = reverse_lazy('bfsapp:admingallerylist')


class AdminGalleryDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bfsapp/admintemplates/admingallerydelete.html'
    model = Gallery
    form_class = AdminGalleryDeleteForm
    success_url = reverse_lazy('bfsapp:admingallerylist')


class AdminNoticeListView(AdminMixin, ListView):
    template_name = 'bfsapp/admintemplates/adminnoticelist.html'
    queryset = Notice.objects.filter(deleted_at__isnull=True).order_by('-id')
    context_object_name = 'allnotices'


class AdminNoticeCreateView(AdminMixin, CreateView):
    template_name = 'bfsapp/admintemplates/adminnoticecreate.html'
    form_class = AdminNoticeCreateForm
    success_url = reverse_lazy('bfsapp:adminnoticelist')


class AdminNoticeUpdateView(AdminMixin, UpdateView):
    template_name = 'bfsapp/admintemplates/adminnoticecreate.html'
    model = Notice
    form_class = AdminNoticeCreateForm
    success_url = reverse_lazy('bfsapp:adminnoticelist')


class AdminNoticeDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bfsapp/admintemplates/adminnoticedelete.html'
    model = Notice
    form_class = AdminNoticeDeleteForm
    success_url = reverse_lazy('bfsapp:adminnoticelist')


class AdminStaffListView(AdminMixin, ListView):
    template_name = 'bfsapp/admintemplates/adminstafflist.html'
    queryset = Staff.objects.filter(deleted_at__isnull=True).order_by('-id')
    context_object_name = 'allstaffs'


class AdminStaffCreateView(AdminMixin, CreateView):
    template_name = 'bfsapp/admintemplates/adminstaffcreate.html'
    form_class = AdminStaffCreateForm
    success_url = reverse_lazy('bfsapp:adminstafflist')


class AdminStaffUpdateView(AdminMixin, UpdateView):
    template_name = 'bfsapp/admintemplates/adminstaffcreate.html'
    model = Staff
    form_class = AdminStaffCreateForm
    success_url = reverse_lazy('bfsapp:adminstafflist')


class AdminStaffDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bfsapp/admintemplates/adminstaffdelete.html'
    model = Staff
    form_class = AdminStaffDeleteForm
    success_url = reverse_lazy('bfsapp:adminstafflist')


class AdminTestimonialListView(AdminMixin, ListView):
    template_name = 'bfsapp/admintemplates/admintestimoniallist.html'
    queryset = Testimonial.objects.filter(
        deleted_at__isnull=True).order_by('-id')
    context_object_name = 'alltestimonials'


class AdminTestimonialCreateView(AdminMixin, CreateView):
    template_name = 'bfsapp/admintemplates/admintestimonialcreate.html'
    form_class = AdminTestimonialCreateForm
    success_url = reverse_lazy('bfsapp:admintestimoniallist')


class AdminTestimonialUpdateView(AdminMixin, UpdateView):
    template_name = 'bfsapp/admintemplates/admintestimonialcreate.html'
    model = Testimonial
    form_class = AdminTestimonialCreateForm
    success_url = reverse_lazy('bfsapp:admintestimoniallist')


class AdminTestimonialDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bfsapp/admintemplates/admintestimonialdelete.html'
    model = Testimonial
    form_class = AdminTestimonialDeleteForm
    success_url = reverse_lazy('bfsapp:admintestimoniallist')



class AdminActivitiesListView(AdminMixin, ListView):
    template_name = 'bfsapp/admintemplates/adminactivitieslist.html'
    queryset = Activities.objects.filter(
        deleted_at__isnull=True).order_by('-id')
    context_object_name = 'allactivities'


class AdminActivitiesCreateView(AdminMixin, CreateView):
    template_name = 'bfsapp/admintemplates/adminactivitiescreate.html'
    form_class = AdminActivitiesCreateForm
    success_url = reverse_lazy('bfsapp:adminactivitieslist')


class AdminActivitiesUpdateView(AdminMixin, UpdateView):
    template_name = 'bfsapp/admintemplates/adminactivitiescreate.html'
    model = Activities
    form_class = AdminActivitiesCreateForm
    success_url = reverse_lazy('bfsapp:adminactivitieslist')


class AdminActivitiesDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bfsapp/admintemplates/adminactivitiesdelete.html'
    model = Activities
    form_class = AdminActivitiesDeleteForm
    success_url = reverse_lazy('bfsapp:adminactivitieslist')


class AdminFeaturesListView(AdminMixin, ListView):
    template_name = 'bfsapp/admintemplates/adminfeatureslist.html'
    queryset = Features.objects.filter(deleted_at__isnull=True).order_by('-id')
    context_object_name = 'allfeatures'


class AdminFeaturesCreateView(AdminMixin, CreateView):
    template_name = 'bfsapp/admintemplates/adminfeaturescreate.html'
    form_class = AdminFeaturesCreateForm
    success_url = reverse_lazy('bfsapp:adminfeatureslist')


class AdminFeaturesUpdateView(AdminMixin, UpdateView):
    template_name = 'bfsapp/admintemplates/adminfeaturescreate.html'
    model = Features
    form_class = AdminFeaturesCreateForm
    success_url = reverse_lazy('bfsapp:adminfeatureslist')


class AdminFeaturesDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bfsapp/admintemplates/adminfeaturesdelete.html'
    model = Features
    form_class = AdminFeaturesDeleteForm
    success_url = reverse_lazy('bfsapp:adminfeatureslist')



class AdminServicesListView(AdminMixin, ListView):
    template_name = 'bfsapp/admintemplates/adminserviceslist.html'
    queryset = Service.objects.filter(deleted_at__isnull=True).order_by('-id')
    context_object_name = 'allservices'


class AdminServicesCreateView(AdminMixin, CreateView):
    template_name = 'bfsapp/admintemplates/adminservicescreate.html'
    form_class = AdminServiceCreateForm
    success_url = reverse_lazy('bfsapp:adminserviceslist')


class AdminServicesUpdateView(AdminMixin, UpdateView):
    template_name = 'bfsapp/admintemplates/adminservicescreate.html'
    model = Service
    form_class = AdminServiceCreateForm
    success_url = reverse_lazy('bfsapp:adminserviceslist')


class AdminServicesDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bfsapp/admintemplates/adminservicesdelete.html'
    model = Service
    form_class = AdminServicesDeleteForm
    success_url = reverse_lazy('bfsapp:adminserviceslist')


class AdminDownloadListView(AdminMixin, ListView):
    template_name = 'bfsapp/admintemplates/admindownloadlist.html'
    queryset = Download.objects.filter(deleted_at__isnull=True).order_by('-id')
    context_object_name = 'alldownloads'


class AdminDownloadCreateView(AdminMixin, CreateView):
    template_name = 'bfsapp/admintemplates/admindownloadcreate.html'
    form_class = AdminDownloadCreateForm
    success_url = reverse_lazy('bfsapp:admindownloadlist')


class AdminDownloadUpdateView(AdminMixin, UpdateView):
    template_name = 'bfsapp/admintemplates/admindownloadcreate.html'
    model = Download
    form_class = AdminDownloadCreateForm
    success_url = reverse_lazy('bfsapp:admindownloadlist')


class AdminDownloadDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bfsapp/admintemplates/admindownloaddelete.html'
    model = Download
    form_class = AdminDownloadDeleteForm
    success_url = reverse_lazy('bfsapp:admindownloadlist')




class AdminRoutineListView(AdminMixin, ListView):
    template_name = 'bfsapp/admintemplates/adminroutinelist.html'
    queryset = Routine.objects.filter(deleted_at__isnull=True)
    context_object_name = 'allroutines'


class AdminRoutineCreateView(AdminMixin, CreateView):
    template_name = 'bfsapp/admintemplates/adminroutinecreate.html'
    form_class = AdminRoutineCreateForm
    success_url = reverse_lazy('bfsapp:adminroutinelist')


class AdminRoutineUpdateView(AdminMixin, UpdateView):
    template_name = 'bfsapp/admintemplates/adminroutinecreate.html'
    model = Routine
    form_class = AdminRoutineCreateForm
    success_url = reverse_lazy('bfsapp:adminroutinelist')


class AdminRoutineDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bfsapp/admintemplates/adminroutinedelete.html'
    model = Routine
    form_class = AdminRoutineDeleteForm
    success_url = reverse_lazy('bfsapp:adminroutinelist')





class AdminResultListView(AdminMixin, ListView):
    template_name = 'bfsapp/admintemplates/adminresultlist.html'
    queryset = Result.objects.filter(deleted_at__isnull=True)
    context_object_name = 'allresults'


class AdminResultCreateView(AdminMixin, CreateView):
    template_name = 'bfsapp/admintemplates/adminresultcreate.html'
    form_class = AdminResultCreateForm
    success_url = reverse_lazy('bfsapp:adminresultlist')


class AdminResultUpdateView(AdminMixin, UpdateView):
    template_name = 'bfsapp/admintemplates/adminresultcreate.html'
    model = Result
    form_class = AdminResultCreateForm
    success_url = reverse_lazy('bfsapp:adminresultlist')


class AdminResultDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bfsapp/admintemplates/adminresultdelete.html'
    model = Result
    form_class = AdminResultDeleteForm
    success_url = reverse_lazy('bfsapp:adminresultlist')



class AdminVacanciiListView(AdminMixin, ListView):
    template_name = 'bfsapp/admintemplates/adminvacanciilist.html'
    queryset = Vacancii.objects.filter(deleted_at__isnull=True)
    context_object_name = 'allVacancis'


class AdminVacanciiCreateView(AdminMixin, CreateView):
    template_name = 'bfsapp/admintemplates/adminvacanciicreate.html'
    form_class = AdminVacanciiCreateForm
    success_url = reverse_lazy('bfsapp:adminvacanciilist')


class AdminVacanciiUpdateView(AdminMixin, UpdateView):
    template_name = 'bfsapp/admintemplates/adminvacanciicreate.html'
    model = Vacancii
    form_class = AdminVacanciiCreateForm
    success_url = reverse_lazy('bfsapp:adminvacanciilist')


class AdminVacanciiDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bfsapp/admintemplates/adminvacanciidelete.html'
    model = Vacancii
    form_class = AdminVacanciiDeleteForm
    success_url = reverse_lazy('bfsapp:adminvacanciilist')




class AdminPriceListView(AdminMixin, ListView):
    template_name = 'bfsapp/admintemplates/adminpricelist.html'
    queryset = Price.objects.filter(deleted_at__isnull=True)
    context_object_name = 'allPrice'


class AdminPriceCreateView(AdminMixin, CreateView):
    template_name = 'bfsapp/admintemplates/adminpricecreate.html'
    form_class = AdminPriceCreateForm
    success_url = reverse_lazy('bfsapp:adminpricelist')


class AdminPriceUpdateView(AdminMixin, UpdateView):
    template_name = 'bfsapp/admintemplates/adminpricecreate.html'
    model = Price
    form_class = AdminPriceCreateForm
    success_url = reverse_lazy('bfsapp:adminpricelist')


class AdminPriceDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bfsapp/admintemplates/adminpricedelete.html'
    model = Price
    form_class = AdminPriceDeleteForm
    success_url = reverse_lazy('bfsapp:adminpricelist')








class AdminSliderListView(AdminMixin, ListView):
    template_name = 'bfsapp/admintemplates/adminsliderlist.html'
    queryset = Slider.objects.filter(deleted_at__isnull=True)
    context_object_name = 'allsliders'


class AdminSliderCreateView(AdminMixin, CreateView):
    template_name = 'bfsapp/admintemplates/adminslidercreate.html'
    form_class = AdminSliderCreateForm
    success_url = reverse_lazy('bfsapp:adminsliderlist')


class AdminSliderUpdateView(AdminMixin, UpdateView):
    template_name = 'bfsapp/admintemplates/adminslidercreate.html'
    model = Slider
    form_class = AdminSliderCreateForm
    success_url = reverse_lazy('bfsapp:adminsliderlist')


class AdminSliderDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bfsapp/admintemplates/adminsliderdelete.html'
    model = Slider
    form_class = AdminSliderDeleteForm
    success_url = reverse_lazy('bfsapp:adminsliderlist')




    



class AdminMessageListView(AdminMixin, ListView):
    template_name= 'bfsapp/admintemplates/adminmessagelist.html'
    queryset= Message.objects.filter(deleted_at__isnull=True).order_by('-id')
    context_object_name= 'allmessage'


class AdminMessageDetailView(AdminMixin, DetailView):
    template_name= 'bfsapp/admintemplates/adminmessagedetail.html'
    queryset= Message.objects.filter(deleted_at__isnull=True)

class AdminMessageDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bfsapp/admintemplates/adminmessagedelete.html'
    model = Message
    form_class = AdminMessageDeleteForm
    success_url = reverse_lazy('bfsapp:adminmessagelist')


class AdminAdmissionListView(AdminMixin, ListView):
    template_name= 'bfsapp/admintemplates/adminadmissionlist.html'
    queryset= Admission.objects.filter(deleted_at__isnull=True).order_by('-id')
    context_object_name= 'alladmission'


class AdminAdmissionDetailView(AdminMixin, DetailView):
    template_name= 'bfsapp/admintemplates/adminadmissiondetail.html'
    queryset= Admission.objects.filter(deleted_at__isnull=True)


class AdminAdmissionDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bfsapp/admintemplates/adminadmissiondelete.html'
    model = Admission
    form_class = AdminAdmissionDeleteForm
    success_url = reverse_lazy('bfsapp:adminadmissionlist')


class AdminVacancyListView(AdminMixin, ListView):
    template_name= 'bfsapp/admintemplates/adminvacancylist.html'
    queryset= Vacancy.objects.filter(deleted_at__isnull=True).order_by('-id')
    context_object_name= 'allvacancy'


class AdminVacancyDetailView(AdminMixin, DetailView):
    template_name= 'bfsapp/admintemplates/adminvacancydetail.html'
    queryset= Vacancy.objects.filter(deleted_at__isnull=True)


class AdminVacancyDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bfsapp/admintemplates/adminvacancydelete.html'
    model = Vacancy
    form_class = AdminVacancyDeleteForm
    success_url = reverse_lazy('bfsapp:adminvacancylist')

# client side

class ClientMixin(object):
    def get_context_data(self, *args, **kwargs):
        context = super(ClientMixin, self).get_context_data(*args, **kwargs)
        context['events'] = Event.objects.filter(deleted_at__isnull=True).order_by('-id')
        context['video'] = Video.objects.filter(deleted_at__isnull=True).order_by('-id')
        context['sliders'] = Slider.objects.filter(deleted_at__isnull=True).order_by('-id')
        context['blogs'] = Blog.objects.filter(deleted_at__isnull=True).order_by('-id')
        context['gallery'] = Gallery.objects.filter(deleted_at__isnull=True).order_by('-id')
        context['notices'] = Notice.objects.filter(deleted_at__isnull=True).order_by('-id')
        context['features'] = Features.objects.filter(deleted_at__isnull=True).order_by('-id')
        context['vacancie'] = Vacancii.objects.filter(deleted_at__isnull=True).order_by('-id')
        context['testimonials'] = Testimonial.objects.filter(
            deleted_at__isnull=True).order_by('-id')
        context['A1s'] = A1.objects.filter(deleted_at__isnull=True)
        context['A2s'] = A2.objects.filter(deleted_at__isnull=True)
        context['A3s'] = A3.objects.filter(deleted_at__isnull=True)
        context['A4s'] = A4.objects.filter(deleted_at__isnull=True)
        context['A5s'] = A5.objects.filter(deleted_at__isnull=True)
        context['A6s'] = A6.objects.filter(deleted_at__isnull=True)
        context['A7s'] = A7.objects.filter(deleted_at__isnull=True)
        context['A8s'] = A8.objects.filter(deleted_at__isnull=True)

        return context



class ClientHomeView(ClientMixin, TemplateView):
	template_name='bfsapp/clienttemplates/clienthome.html'

class ClientExamView(ClientMixin, TemplateView):
    template_name='bfsapp/clienttemplates/clientexam.html'

class ClientHomeworkView(ClientMixin, TemplateView):
    template_name='bfsapp/clienttemplates/clienthomework.html'



class ClientAboutView(ClientMixin, ListView):
    template_name = 'bfsapp/clienttemplates/clientabout.html'
    queryset = Staff.objects.filter(deleted_at__isnull=True)
    context_object_name = 'staffs'


class ClientServiceListView(ClientMixin, ListView):
    template_name = 'bfsapp/clienttemplates/clientservicelist.html'
    queryset = Service.objects.filter(deleted_at__isnull=True)
    context_object_name = 'services'




class ClientGalleryListView(ClientMixin, ListView):
    template_name = 'bfsapp/clienttemplates/clientgallerylist.html'
    queryset = Gallery.objects.filter(deleted_at__isnull=True)


class ClientBlogListView(ClientMixin, ListView):
    template_name = 'bfsapp/clienttemplates/clientbloglist.html'
    queryset = Blog.objects.filter(deleted_at__isnull=True)
    context_object_name ='blogs'


class ClientBlogDetailView(ClientMixin, DetailView):
    template_name = 'bfsapp/clienttemplates/clientblogdetail.html'
    model = Blog
    context_object_name = 'abc'



class ClientActivitiesListView(ClientMixin, ListView):
    template_name = 'bfsapp/clienttemplates/clientactivitieslist.html'
    queryset = Activities.objects.filter(deleted_at__isnull=True)
    context_object_name ='activities'


class ClientActivitiesDetailView(ClientMixin, DetailView):
    template_name = 'bfsapp/clienttemplates/clientactivitiesdetail.html'
    model = Activities


class ClientContactView(ClientMixin, CreateView):
    template_name = 'bfsapp/clienttemplates/clientcontact.html'
    form_class = MessageForm
    success_url = '/thankum/'

    def form_valid(self, form):
        name = form.cleaned_data.get('name')
        phone = form.cleaned_data.get('phone')
        subject = form.cleaned_data.get('subject')
        to_email = form.cleaned_data.get('email')
        message = form.cleaned_data.get('message')
        # body = "From: " + name + "(" + from_email + ")" + "\n" + "Message:-" + message + "\n" + "Phone:- " + phone
        body = "Thank you for messaging us. We will contact you soon"
        from_email = settings.EMAIL_HOST_USER
        send_mail(
            subject,
            body,
            from_email,
            [to_email]
        )
        return super().form_valid(form)


class ClientAdmissionnView(ClientMixin, CreateView):
    template_name = 'bfsapp/clienttemplates/clientadmissionn.html'
    form_class = AdmissionForm
    success_url = '/thankua/'



class ClientEventDetailView(ClientMixin, DetailView):
    template_name= 'bfsapp/clienttemplates/clienteventdetail.html'
    model = Event



class ClientAdmissionView(ClientMixin, TemplateView):
    template_name='bfsapp/clienttemplates/clientadmission.html'

class ClientRuppeshView(ClientMixin, TemplateView):
    template_name='bfsapp/clienttemplates/clientruppesh.html'


class ClientFeestructureView(ClientMixin, ListView):
    template_name='bfsapp/clienttemplates/clientfeestructure.html'
    queryset = Price.objects.filter(deleted_at__isnull=True).order_by('-id')
    context_object_name ='prices'


class ClientDownloadListView(ClientMixin, ListView):
    template_name='bfsapp/clienttemplates/ClientdownloadList.html'
    queryset = Download.objects.filter(deleted_at__isnull=True).order_by('-id')
    context_object_name = 'downloads'


class ClientRoutineListView(ClientMixin, ListView):
    template_name='bfsapp/clienttemplates/Clientroutinelist.html'
    queryset = Routine.objects.filter(deleted_at__isnull=True).order_by('-id')
    context_object_name ='routines'


class ClientResultListView(ClientMixin, ListView):
    template_name = 'bfsapp/clienttemplates/Clientresultlist.html'
    queryset = Result.objects.filter(deleted_at__isnull=True).order_by('-id')
    context_object_name ='results'

class ClientVacancyListView(ClientMixin, CreateView):
    template_name = 'bfsapp/clienttemplates/clientvacancylist.html'
    form_class = VacancyForm
    context_object_name ='vacancys'
    success_url = '/thankuv/'



class ClientSearchView(ClientMixin, TemplateView):
    template_name ='bfsapp/clienttemplates/clientsearch.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'search' in self.request.GET:
            data = self.request.GET['search']
            context['searched_blogs'] = Blog.objects.filter(
                deleted_at__isnull=True).filter(Q(title__icontains=data) | Q(
                    content__icontains=data))
            context['searched_notices'] = Notice.objects.filter(
                deleted_at__isnull=True).filter(Q(title__icontains=data) | Q(
                    content__icontains=data))
            context['searched_events'] = Event.objects.filter(
                deleted_at__isnull=True).filter(Q(title__icontains=data) | Q(
                    content__icontains=data)) 
            context['searched_staffs'] = Staff.objects.filter(
                deleted_at__isnull=True).filter(Q(name__icontains=data) | Q(
                    post__icontains=data))
            context['searched_services'] = Service.objects.filter(
                deleted_at__isnull=True).filter(Q(title__icontains=data) | Q(
                    content__icontains=data))

        return context


class ClientThankumView(ClientMixin, TemplateView):
    template_name='bfsapp/clienttemplates/clientthankum.html'


class ClientThankuaView(ClientMixin, TemplateView):
    template_name='bfsapp/clienttemplates/clientthankua.html'


class ClientThankuvView(ClientMixin, TemplateView):
    template_name='bfsapp/clienttemplates/clientthankuv.html'

class HomeView(ClientMixin, TemplateView):
    template_name='bfsapp/clienttemplates/home.html'


class ClientBookView(ClientMixin, TemplateView):
    template_name='bfsapp/clienttemplates/clientbook.html'

class ClientSolutionView(ClientMixin, TemplateView):
    template_name='bfsapp/clienttemplates/clientsolution.html'

