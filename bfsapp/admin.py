from django.contrib import admin
from .models import *


admin.site.register([Blog, Event, Notice, Staff, Features, 
	Gallery, Activities, Slider, Service, 
	Download, Testimonial, Message, Result, Routine, Admission, Vacancy, Vacancii, Price,
	A1, A2, A3, A4, A5, A6, A7, A8])
